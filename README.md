# check_release.pl - monitor releases of manually installed software

A simple script to monitor manually installed software for new upstream
releases. Can be used as monitoring plugin with Icinga/Nagios/...

It takes a local changelog file and a remote upstream release page as
input, guesses the highest version from both and compares them.

Optionally, the remote upstream release page can be cached for one day.

## Perl dependencies

* `Dpkg::Version`  (Debian package `libdpkg-perl`)
* `HTTP::Request`  (Debian package `libhttp-message-perl`)
* `LWP::UserAgent` (Debian package `libwww-perl`)

## Usage examples

```
check_release.pl --name=roundcube --changelog=/var/www/roundcube/CHANGELOG \
        --url='https://github.com/roundcube/roundcubemail/releases' \
        --regex='roundcubemail/releases/tag/([^v]__VER__)'
```

```
check_release.pl --name=linux --changelog=/usr/src/linux/ChangeLog \
        --url='https://www.kernel.org/pub/linux/kernel/v4.x/' \
        --regex='linux-(__VER__)\.tar --cache'
```

```
check_release.pl --name=app --changelog=/usr/local/app/CHANGELOG \
        --url='http://sourceforge.net/projects/app/files/app/' \
        --regex='app/files/app/([^v]__VER__)/'
```

## License

GNU General Public License, Version 2 or later
